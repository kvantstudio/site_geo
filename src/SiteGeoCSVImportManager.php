<?php

namespace Drupal\site_geo;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an SiteGeoCSVImport plugin manager.
 */
class SiteGeoCSVImportManager extends DefaultPluginManager {
  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteGeoCSVImportPlugin',
      $namespaces,
      $module_handler,
      'Drupal\site_geo\SiteGeoCSVImportPluginInterface',
      'Drupal\site_geo\Annotation\SiteGeoCSVImportPlugin'
    );
    # hook_site_geo_info_alter();
    $this->alterInfo('site_geo_info');
    $this->setCacheBackend($cache_backend, 'site_geo');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}
