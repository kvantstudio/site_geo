<?php

namespace Drupal\site_geo;

use Drupal\Core\Database\Database;
use Drupal\file\Entity\File;

/**
 * Class SiteGeoCSVBatchImport.
 */
class SiteGeoCSVBatchImport {
  # Здесь мы будем хранить всю информацию о нашей Batch операции.
  private $batch;

  # FID для CSV файла.
  private $fid;

  # Объект файла.
  private $file;

  # Мы также добавим возможность игнорировать первую строку у csv файла,
  # которая может использоваться для заголовков столбцов.
  # По умолчанию первая линия будет считываться и обрабатываться.
  private $skip_first_line;

  # Разделитель столбцов CSV.
  private $delimiter;

  # Ограничитель поля CSV.
  private $enclosure;

  # Добавляем переменную для хранение размера чанка
  # к остальным.
  private $chunk_size;

  # Название плагина для импорта.
  private $importPluginId;

  # Объект плагина.
  private $importPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $fid, $skip_first_line = FALSE, $delimiter = ';', $enclosure = ',', $chunk_size = 20, $batch_name = 'CSV import') {
    $this->importPluginId = $plugin_id;
    $this->importPlugin = \Drupal::service('plugin.manager.site_geo')->createInstance($plugin_id);
    $this->fid = $fid;
    $this->file = File::load($fid);
    $this->skip_first_line = $skip_first_line;
    $this->delimiter = $delimiter;
    $this->enclosure = $enclosure;
    $this->chunk_size = $chunk_size;
    $this->batch = [
      'title' => $batch_name,
      'finished' => [$this, 'finished'],
      'file' => drupal_get_path('module', 'site_geo') . '/src/SiteGeoCSVBatchImport.php',
    ];
    $this->parseCSV();
  }

  /**
   * {@inheritdoc}
   */
  public function setOperation($data) {
    $this->batch['operations'][] = [
      [$this->importPlugin, 'processItem'],
      [$data],
    ];
  }

  /**
   * {@inheritdoc}
   * Метод для регистрации нашей batch операции, которую мы подготовили.
   */
  public function setBatch() {
    batch_set($this->batch);
  }

  /**
   * {@inheritdoc}
   * Метод для ручного запуска выполнения batch операций.
   */
  public function processBatch() {
    batch_process();
  }

  /**
   * {@inheritdoc}
   *
   * Метод который будет вызван по окончанию всех batch операций, или в случае
   * возникновения ошибки в процессе.
   */
  public function finished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results), 'One post processed.', '@count posts processed.');
    } else {
      $message = $this->t('Finished with an error.');
    }
    drupal_set_message($message);
  }

  /**
   * {@inheritdoc}
   *
   * В данном методе мы обрабатываем наш CSV строка за строкой, а не грузим
   * весь файл в память, так что данный способ значительно менее затратный
   * и более шустрый.
   *
   * Каждую строку мы получаем в виде массива, а массив передаем в операцию на
   * выполнение.
   */
  public function parseCSV() {
    # Создаем массив для данных csv.
    $items = [];
    if (($handle = fopen($this->file->getFileUri(), 'r')) !== FALSE) {
      if ($this->skip_first_line) {
        fgetcsv($handle, 0, ';');
      }
      while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {
        # Данные мы теперь не устанавливаем на операцию,
        # а сохраняем в массив для дальнейшего дробления.
        $items[] = $data;
      }
      fclose($handle);
    }

    # После того как распарсили файл в массив, мы разбиваем его на кусочки.
    $chunks = array_chunk($items, $this->chunk_size);

    # Теперь каждый кусок устанавливаем на выполнение.
    foreach ($chunks as $chunk) {
      $this->setOperation($chunk);
    }

    // Очищает таблицу перед импортом.
    $database = Database::getConnection();
    $database->delete($this->importPluginId)->execute();
  }
}