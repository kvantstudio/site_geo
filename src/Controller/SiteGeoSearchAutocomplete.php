<?php
/**
 * @file
 * Contains \Drupal\site_geo\Controller\SiteGeoSearchAutocomplete.
 */

namespace Drupal\site_geo\Controller;

use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SiteGeoSearchAutocomplete {

  /**
   * Поиск результатов для autocomplete поля.
   *
   * {@inheritdoc}
   */
  public function autocomplete(Request $request, $table = NULL, $id = NULL) {
    $string = $request->query->get('q');

    $matches = [];
    $matches_id = [];

    if ($string) {
      $string_array = explode(" ", $string);
      $request = '';
      $arguments = array();
      $i = 1;
      $count_array_string = count($string_array);

      foreach ($string_array as $key) {
        $arguments[':key' . $i] = $key;
        if ($count_array_string > $i) {
          $request .= 'INSTR(i.name, :key' . $i . ') > 0 AND ';
        } else {
          $request .= 'INSTR(i.name, :key' . $i . ') > 0';
        }
        $i++;
      }

      $query = \Drupal::database()->select($table, 'i');
      $query->fields('i', array($id, 'name'));
      $query->where($request, $arguments);
      $query->orderBy('i.name', 'ASC');
      $query->range(0, 10);
      $result = $query->execute();

      foreach ($result as $row) {
        if (!array_key_exists($row->$id, $matches_id)) {
          $matches_id[$row->$id] = $row->name;
          $matches[] = ['value' => $row->name, 'label' => $row->name];
        }
      }
    }

    return new JsonResponse($matches);
  }
}