<?php

namespace Drupal\site_geo\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the site_geo_location field type.
 *
 * @FieldType(
 *   id = "site_geo_location",
 *   label = @Translation("Location"),
 *   module = "site_geo",
 *   category = @Translation("Location"),
 *   description = @Translation(""),
 *   default_widget = "site_geo_location_default_widget",
 *   default_formatter = "site_geo_location_default_formatter"
 * )
 */
class SiteGeoLocation extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['country'] = DataDefinition::create('string')->setLabel(t('Country'))->setRequired(FALSE);
    $properties['region'] = DataDefinition::create('string')->setLabel(t('Region'))->setRequired(FALSE);
    $properties['city'] = DataDefinition::create('string')->setLabel(t('City'))->setRequired(FALSE);
    $properties['latitude'] = DataDefinition::create('string')->setLabel(t('Latitude'))->setRequired(FALSE);
    $properties['longitude'] = DataDefinition::create('string')->setLabel(t('Longitude'))->setRequired(FALSE);
    $properties['address'] = DataDefinition::create('string')->setLabel(t('Address'))->setRequired(FALSE);
    $properties['metro'] = DataDefinition::create('string')->setLabel(t('Metro station'))->setRequired(FALSE);
    $properties['description'] = DataDefinition::create('string')->setLabel(t('Description'))->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'country' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'region' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'city' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'latitude' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'longitude' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'address' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'metro' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ),
        'description' => array(
          'type' => 'text',
          'not null' => FALSE,
        ),
      ),
      'indexes' => [],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = TRUE;
    if ($this->get('country')->getValue() || $this->get('region')->getValue() || $this->get('city')->getValue() || $this->get('address')->getValue() || $this->get('metro')->getValue() || $this->get('description')->getValue()) {
      $isEmpty = FALSE;
    }
    return $isEmpty;
  }
}