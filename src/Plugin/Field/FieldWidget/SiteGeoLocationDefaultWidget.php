<?php

namespace Drupal\site_geo\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the site_geo_location_default_widget.
 *
 * @FieldWidget(
 *   id = "site_geo_location_default_widget",
 *   module = "site_geo",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_geo_location"
 *   }
 * )
 */
class SiteGeoLocationDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['title'] = array(
      '#markup' => '<h2>' . $this->t('Location') . '</h2>',
    );

    $element['country'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => isset($items[$delta]->country) ? $items[$delta]->country : null,
      '#autocomplete_route_name' => 'site_geo.country_search_autocomplete',
      '#maxlength' => 255,
    );

    $element['region'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#default_value' => isset($items[$delta]->region) ? $items[$delta]->region : null,
      '#autocomplete_route_name' => 'site_geo.region_search_autocomplete',
      '#maxlength' => 255,
    );

    $element['city'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => isset($items[$delta]->city) ? $items[$delta]->city : null,
      '#autocomplete_route_name' => 'site_geo.city_search_autocomplete',
      '#maxlength' => 255,
    );

    $element['address'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#default_value' => isset($items[$delta]->address) ? $items[$delta]->address : null,
      '#empty_value' => '',
      '#maxlength' => 255,
    );

    $element['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('A note to the address'),
      '#default_value' => isset($items[$delta]->description) ? $items[$delta]->description : null,
      '#empty_value' => '',
      '#maxlength' => 255,
    );

    return $element;
  }
}
