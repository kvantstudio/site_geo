<?php
namespace Drupal\site_geo\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the site_geo_location_default_formatter.
 *
 * @FieldFormatter(
 *   id = "site_geo_location_default_formatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_geo_location"
 *   }
 * )
 */
class SiteGeoLocationDefaultFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = []; $values = [];

    $fields = [
      'country' => $this->t('Country'),
      'region' => $this->t('Region'),
      'city' => $this->t('City'),
      'address' => $this->t('Address'),
      'description' => $this->t('Description'),
    ];

    foreach ($items as $delta => $item) {
      foreach ($fields as $field => $label) {
        if (!empty($value = $item->$field)) {
            $values[$field] = ['label' => $label, 'value' => $value];
        }
      }
    }

    \Drupal::moduleHandler()->alter('hidden_fields', $values);

    $elements[] = [
      '#theme' => 'site_geo_location_default_formatter',
      '#values' => $values,
      '#title' => $this->t('Location'),
    ];

    return $elements;
  }
}
