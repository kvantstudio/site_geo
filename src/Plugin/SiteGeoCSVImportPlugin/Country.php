<?php

namespace Drupal\site_geo\Plugin\SiteGeoCSVImportPlugin;

use Drupal\Core\Annotation\Translation;
use Drupal\site_geo\Annotation\SiteGeoCSVImportPlugin;
use Drupal\site_geo\SiteGeoCSVImportPluginBase;
use Drupal\Core\Database\Database;

/**
 * Class Country
 * @package Drupal\site_geo\Plugin\SiteGeoCSVImportPlugin
 *
 * @SiteGeoCSVImportPlugin(
 *   id = "site_geo_country",
 *   label = @Translation("Country")
 * )
 */
class Country extends SiteGeoCSVImportPluginBase {
    /**
     * {@inheritdoc}
     *
     * Обработка элемента (строки из файла). В соответствии со столбцами и их
     * порядком мы получаем их данные в переменные. И не забываем про $context.
     */
    public function processItem($data, &$context) {
        $database = Database::getConnection();

        # Прокручиваем $data, каждый элемент которого
        # является массивом с данными из CSV.
        foreach ($data as $item) {
            # В этой строке мы лишь заменяем $data на $item,
            # в соответствии с циклом выше.
            list($country_id, $name) = $item;

            $database->insert('site_geo_country')
                ->fields([
                    'country_id' => $country_id,
                    'name' => $name,
                ])
                ->execute();

            # Записываем результат в общий массив результатов batch операции. По этим
            # данным мы будем выводить кол-во импортированных данных.
            $context['results'][] = $country_id . ' : ' . $name;
            $context['message'] = $name;
        }
    }
}