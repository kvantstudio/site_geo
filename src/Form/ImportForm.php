<?php

namespace Drupal\site_geo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\site_geo\SiteGeoCSVBatchImport;

/**
 * Class ImportForm.
 *
 * @package Drupal\site_geo\Form
 */
class ImportForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function __construct(\Drupal\Core\Config\ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginList() {
    $definitions = \Drupal::service('plugin.manager.site_geo')->getDefinitions();
    $plugin_list = [];
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $plugin['label']->render();
    }
    return $plugin_list;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['site_geo.import'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_geo_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_geo.import');

    $form['file'] = [
      '#title' => $this->t('CSV file'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://',
      '#default_value' => $config->get('fid') ? [$config->get('fid')] : NULL,
      '#upload_validators' => array(
        'file_validate_extensions' => array('csv'),
      ),
      '#required' => TRUE,
    ];

    // Если файл был загружен.
    if (!empty($config->get('fid'))) {
      $file = File::load($config->get('fid'));
      $created = \Drupal::service('date.formatter')->format($file->created->value, 'medium');

      $form['file_information'] = [
        '#markup' => $this->t('This file was uploaded at @created.', ['@created' => $created]),
      ];

      // Кнопка импорта со своим собственным обработчиком.
      $form['actions']['start_import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Start import'),
        '#submit' => ['::startImport'],
        '#weight' => 100,
      ];
    }

    $form['import_plugin'] = [
      '#title' => $this->t('Select type to import'),
      '#type' => 'select',
      '#options' => $this->getPluginList(),
      '#empty_option' => $this->t('None'),
      '#element_validate' => [
        [$this, 'validateImportPlugin'],
      ],
    ];

    $form['additional_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional settings'),
    ];

    $form['additional_settings']['skip_first_line'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip first line'),
      '#default_value' => $config->get('skip_first_line'),
      '#description' => $this->t('If file contain titles, this checkbox help to skip first line.'),
    ];

    $form['additional_settings']['delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter'),
      '#default_value' => $config->get('delimiter'),
      '#required' => TRUE,
    ];

    $form['additional_settings']['enclosure'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enclosure'),
      '#default_value' => $config->get('enclosure'),
      '#required' => TRUE,
    ];

    $form['additional_settings']['chunk_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Chunk size'),
      '#default_value' => $config->get('chunk_size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateImportPlugin(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] == 'start_import') {
      $plugin_id = $form_state->getValue('import_plugin');
      if ($plugin_id == "") {
        $form_state->setErrorByName('import_plugin', $this->t('You must select type to import.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getTriggeringElement()['#name'] == 'start_import') {
      $plugin_id = $form_state->getValue('import_plugin');
      if ($plugin_id == "") {
        $form_state->setErrorByName('import_plugin', $this->t('You must select type to import.'));
      }

      if ($form_state->getValue('chunk_size') < 1) {
        $form_state->setErrorByName('chunk_size', $this->t('Chunk size must be greater or equal 1.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('site_geo.import');

    // Сохраняем идентификатор файла.
    $fid_old = $config->get('fid');
    $fid_form = $form_state->getValue('file')[0];

    // Проверяет отличие текущего файла (ранее загруженного) от нового.
    if (empty($fid_old) || $fid_old != $fid_form) {
      // Удаляет старый файл.
      if (!empty($fid_old)) {
        $previous_file = File::load($fid_old);
        \Drupal::service('file.usage')->delete($previous_file, 'site_geo', 'config_form', $previous_file->id());
      }

      // Сохраняет новый файл.
      $new_file = File::load($fid_form);
      $new_file->save();

      // Устанавливает принадлежность файла, чтобы предотвартить его удаление по Cron.
      \Drupal::service('file.usage')->add($new_file, 'site_geo', 'config_form', $new_file->id());

      // Сохраняет информацию в конфигурацию.
      $config->set('fid', $fid_form)->set('creation', time());
    }

    $config->set('skip_first_line', $form_state->getValue('skip_first_line'))
      ->set('delimiter', $form_state->getValue('delimiter'))
      ->set('enclosure', $form_state->getValue('enclosure'))
      ->set('chunk_size', $form_state->getValue('chunk_size'))
      ->save();
  }

  /**
   * {@inheritdoc}
   *
   * Импорт из файла.
   */
  public function startImport(array &$form, FormStateInterface $form_state) {
    // Выполняет стандартную валидацию полей формы и добавляет примечания об ошибках.
    ConfigFormBase::validateForm($form, $form_state);

    if (!$form_state->getValue('validate_error')) {
      $config = $this->config('site_geo.import');

      $fid = $config->get('fid');
      $skip_first_line = $config->get('skip_first_line');
      $delimiter = $config->get('delimiter');
      $enclosure = $config->get('enclosure');
      $chunk_size = $config->get('chunk_size');
      $plugin_id = $form_state->getValue('import_plugin');

      if ($plugin_id != "") {
        $import = new SiteGeoCSVBatchImport($plugin_id, $fid, $skip_first_line, $delimiter, $enclosure, $chunk_size);
        $import->setBatch();
      }
    }
  }
}