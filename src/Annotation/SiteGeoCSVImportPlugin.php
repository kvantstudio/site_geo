<?php

namespace Drupal\site_geo\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotations for SiteGeoCSVImportPlugin.
 *
 * @Annotation
 */
class SiteGeoCSVImportPlugin extends Plugin {

  /**
   * The plugin ID.
   */
  public $id;

  /**
   * Label will be used in interface.
   */
  public $label;

}
