<?php

/**
 * @file
 * Hooks provided by the site_geo module.
 */

/**
 * Declare a compatible field type for use with File (Field) Paths.
 *
 * @return array
 *   An array of field types.
 */
function hook_filefield_paths_field_type_info() {
  return array('file');
}